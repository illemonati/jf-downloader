import { ipcRenderer } from "electron";
import * as auth from "./auth";
import * as media from "./media";

export default {
    auth,
    media,
};
