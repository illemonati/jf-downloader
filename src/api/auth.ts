import { ipcRenderer } from "electron";

export const login = async (username: string, pw: string, serverURL?: string) =>
    await ipcRenderer.invoke("auth:login", username, pw, serverURL);
