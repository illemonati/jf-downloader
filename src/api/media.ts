import { ipcRenderer } from "electron";
import { Views } from "../types/views";
import { ItemsResult } from "../types/items";

export const getViews = async (): Promise<Views> =>
    await ipcRenderer.invoke("media:get-views");

export const getPrimaryImage = async (itemID: string): Promise<string> => {
    return await ipcRenderer.invoke("media:get-primary-image", itemID);
};

export const getItems = async (
    parentID?: string,
    itemIDs?: string[],
    recursive?: boolean
): Promise<ItemsResult> => {
    return await ipcRenderer.invoke("media:get-items", parentID);
};

export const download = async (itemID: string) =>
    await ipcRenderer.invoke("media:download", itemID);
