import axios from "axios";
import os from "os";
import path from "path";
import fs from "fs";
import { Item, ItemsResult } from "../types/items";
import { JFDownloadMeta } from "./filemeta";
import { sleep } from "../utils";
import stream from "stream";
import embyHeaders from "./emby-headers-conf.json";

export class JellyFinManager {
    private _authToken = "";
    private _serverURL = "";

    public baseDIR = path.join(os.homedir(), "jf-downloader");

    public userID = "";

    public constructor(serverURL?: string) {
        this.authToken = "";
    }

    public set authToken(token: string) {
        this._authToken = token;
        console.log(token);

        let embyAuthString = ``;
        for (let [key, val] of Object.entries(embyHeaders)) {
            embyAuthString += `${key}="${val}", `;
        }
        if (token) {
            embyAuthString += `Token="${token}", `;
        }

        embyAuthString = embyAuthString.substring(0, embyAuthString.length - 2);

        console.log(embyAuthString);
        axios.defaults.headers["x-emby-authorization"] = embyAuthString;
    }

    public set serverURL(url: string) {
        this._serverURL = url;
        axios.defaults.baseURL = url;
    }

    login = async (username: string, pw: string, serverURL?: string) => {
        serverURL && (this.serverURL = serverURL);

        try {
            const res = await axios.post(
                "/Users/authenticatebyname",
                {
                    Username: username,
                    Pw: pw,
                },
                {
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            this.authToken = res.data["AccessToken"];
            this.userID = res.data["User"]["Id"];
            return { err: null };
        } catch (e) {
            return { err: e };
        }
    };

    getViews = async () => {
        const res = await axios.get(`/Users/${this.userID}/Views`);
        return res.data;
    };

    getPrimaryImage = async (itemID: string) => {
        const res = await axios.get(`/Items/${itemID}/Images/Primary`, {
            responseType: "arraybuffer",
        });
        const b64 = Buffer.from(res.data).toString("base64");
        const imageURL =
            "data:" + res.headers["content-type"] + ";base64," + b64;
        return imageURL;
    };

    getItems = async (
        parentID?: string,
        itemID?: string,
        recursive: boolean = false
    ) => {
        const params: { [key: string]: string | string[] } = {
            SortBy: "SortName",
            SortOrder: "Ascending",
            Recursive: recursive ? "True" : "False",
        };
        let path = `/Users/${this.userID}/Items`;
        if (parentID) {
            params.ParentID = parentID;
        }
        if (itemID) {
            path += `/${itemID}`;
        }

        console.log(params);
        const res = await axios.get(path, {
            params,
        });
        return res.data;
    };

    download = async (itemID: string) => {
        const item: Item = await this.getItems(undefined, itemID);
        const itemFolderPath = path.join(this.baseDIR, itemID);
        await fs.promises.mkdir(itemFolderPath, { recursive: true });

        const metadataPath = path.join(
            itemFolderPath,
            "jf-downloader-meta.json"
        );

        const downloadMeta: JFDownloadMeta = {
            itemDetails: item,
            isFolder: false,
        };

        if (item.IsFolder) {
            const items: ItemsResult = await this.getItems(item.Id);
            downloadMeta.isFolder = true;

            await fs.promises.writeFile(
                metadataPath,
                JSON.stringify(item, null, 2)
            );
            for (const subitem of items.Items) {
                const subItemPath = path.join(this.baseDIR, subitem.Id);
                await fs.promises.mkdir(subItemPath, { recursive: true });
                try {
                    await fs.promises.symlink(
                        subItemPath,
                        path.join(itemFolderPath, subitem.Id)
                    );
                } catch (e) {
                    console.log(`Could not symlink ${subItemPath}`);
                }
                this.download(subitem.Id);
                await sleep(1000);
            }
            return;
        }

        const filePath = path.join(itemFolderPath, path.basename(item.Path));

        downloadMeta.downloadInfo = {
            completed: false,
            progress: 0,
            length: 0,
            written: 0,
        };

        if (fs.existsSync(metadataPath)) {
            const diskfileMeta: JFDownloadMeta = JSON.parse(
                await fs.promises.readFile(metadataPath, "utf-8")
            );
            downloadMeta.downloadInfo = diskfileMeta.downloadInfo;
        }

        fs.writeFileSync(metadataPath, JSON.stringify(downloadMeta, null, 4));

        if (downloadMeta.downloadInfo.completed === true) {
            return;
        }

        const writeStream = fs.createWriteStream(filePath, { flags: "w" });

        const resp = await axios.get(`/Items/${itemID}/File`, {
            responseType: "stream",
        });

        console.log(resp.headers);

        downloadMeta.downloadInfo.progress = 0;
        downloadMeta.downloadInfo.length = parseInt(
            (resp.headers["content-length"] as string) || "0"
        );

        console.log(resp.headers["content-length"] as string);
        downloadMeta.downloadInfo.written = 0;

        writeStream.on("close", () => {
            downloadMeta.downloadInfo.completed = true;
            downloadMeta.downloadInfo.progress = 1;
            fs.writeFileSync(
                metadataPath,
                JSON.stringify(downloadMeta, null, 4)
            );
        });

        (resp.data as stream.Readable).on("data", (chunk) => {
            // console.log(chunk);
            // console.log(chunk.length);
            downloadMeta.downloadInfo.written += chunk.length;
            if (downloadMeta.downloadInfo.length) {
                downloadMeta.downloadInfo.progress =
                    downloadMeta.downloadInfo.written /
                    downloadMeta.downloadInfo.length;
            }
            fs.writeFileSync(
                metadataPath,
                JSON.stringify(downloadMeta, null, 4)
            );
        });

        resp.data.pipe(writeStream);
    };
}
