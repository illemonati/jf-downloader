import { Item } from "../types/items";

export type JFDownloadMeta = {
    downloadInfo?: {
        completed: boolean;
        progress: number;
        length: number;
        written: number;
    };
    isFolder: boolean;
    itemDetails: Item;
};
