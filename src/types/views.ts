export interface Views {
    Items: Item[];
    TotalRecordCount: number;
    StartIndex: number;
}

export interface Item {
    Name: string;
    ServerId: string;
    Id: string;
    Etag: string;
    DateCreated: string;
    CanDelete: boolean;
    CanDownload: boolean;
    SortName: string;
    ExternalUrls: any[];
    Path: string;
    EnableMediaSourceDisplay: boolean;
    ChannelId: any;
    Taglines: any[];
    Genres: any[];
    PlayAccess: string;
    RemoteTrailers: any[];
    ProviderIds: ProviderIds;
    IsFolder: boolean;
    ParentId: string;
    Type: string;
    People: any[];
    Studios: any[];
    GenreItems: any[];
    LocalTrailerCount: number;
    UserData: UserData;
    ChildCount: number;
    SpecialFeatureCount: number;
    DisplayPreferencesId: string;
    Tags: any[];
    PrimaryImageAspectRatio: number;
    CollectionType: string;
    ImageTags: ImageTags;
    BackdropImageTags: any[];
    ImageBlurHashes: ImageBlurHashes;
    LocationType: string;
    LockedFields: any[];
    LockData: boolean;
}

export interface ProviderIds {}

export interface UserData {
    PlaybackPositionTicks: number;
    PlayCount: number;
    IsFavorite: boolean;
    Played: boolean;
    Key: string;
}

export interface ImageTags {
    Primary: string;
}

export interface ImageBlurHashes {
    Primary: {
        [key: string]: string;
    };
}
