export interface ItemsResult {
    Items: Item[];
    TotalRecordCount: number;
    StartIndex: number;
}

export interface Item {
    Name: string;
    ServerId: string;
    Id: string;
    Etag: string;
    DateCreated: string;
    CanDelete: boolean;
    CanDownload: boolean;
    SortName: string;
    PremiereDate: string;
    ExternalUrls: any[];
    Path: string;
    EnableMediaSourceDisplay: boolean;
    ChannelId: any;
    Taglines: any[];
    Genres: any[];
    PlayAccess: string;
    ProductionYear: number;
    IndexNumber: number;
    RemoteTrailers: any[];
    ProviderIds: ProviderIds;
    IsFolder: boolean;
    ParentId: string;
    Type: string;
    People: People[];
    Studios: any[];
    GenreItems: any[];
    ParentLogoItemId: string;
    ParentBackdropItemId: string;
    ParentBackdropImageTags: string[];
    LocalTrailerCount: number;
    UserData: UserData;
    RecursiveItemCount: number;
    ChildCount: number;
    SeriesName: string;
    SeriesId: string;
    SpecialFeatureCount: number;
    DisplayPreferencesId: string;
    Tags: any[];
    PrimaryImageAspectRatio: number;
    SeriesPrimaryImageTag: string;
    ImageTags: ImageTags;
    BackdropImageTags: any[];
    ParentLogoImageTag: string;
    ImageBlurHashes: ImageBlurHashes;
    SeriesStudio: string;
    LocationType: string;
    LockedFields: any[];
    LockData: boolean;
}

export interface ProviderIds {
    Tvdb: string;
}

export interface People {
    Name: string;
    Id: string;
    Role: string;
    Type: string;
    PrimaryImageTag?: string;
    ImageBlurHashes: any[];
}

export interface Primary {
    afa520ed64305db46a72dc936b662425: string;
    "9467664e91ac61b1ade80029ef154728": string;
}

export interface Logo {
    c2f7d7aa2f24227809a0df1a15249dfb: string;
}

export interface Backdrop {
    d8efad9484ebe4d632d08d1cf7048eed: string;
}

export interface UserData {
    PlaybackPositionTicks: number;
    PlayCount: number;
    IsFavorite: boolean;
    Played: boolean;
    Key: string;
    UnplayedItemCount?: number;
    LastPlayedDate?: string;
    PlayedPercentage?: number;
}

export interface ImageTags {
    Primary?: string;
    Banner?: string;
    Logo?: string;
}

export interface ImageBlurHashes {
    Primary?: Primary;
    Logo?: Logo;
    Backdrop?: Backdrop;
    Banner?: Banner;
}

export interface Primary {
    [key: string]: string;
}

export interface Logo {
    [key: string]: string;
}

export interface Backdrop {
    [key: string]: string;
}

export interface Banner {
    [key: string]: string;
}
