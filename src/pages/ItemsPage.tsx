import {
    Container,
    Typography,
    Paper,
    Box,
    Stack,
    Button,
} from "@mui/material";
import Grid from "@mui/material/Unstable_Grid2/Grid2";
import React from "react";
import { useAsync } from "react-use";
import Loading from "../components/Loading";
import { Item } from "../types/items";
import SyntaxHighlighter from "react-syntax-highlighter/dist/esm/default-highlight";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

import { useParams } from "react-router-dom";

type ItemsPageParams = {
    parentID?: string;
};

const ItemsPage: React.FC = () => {
    const { parentID } = useParams<ItemsPageParams>();
    const navigate = useNavigate();

    const items = useAsync(async () => {
        return await api.media.getItems(parentID);
    }, [parentID]);
    console.log(items);
    return (
        <Box margin={2}>
            {items.loading ? (
                <Loading />
            ) : (
                <Stack gap={3}>
                    <Typography variant="h4">
                        Total Record Count: {items.value.TotalRecordCount}
                    </Typography>
                    <Button
                        variant="contained"
                        sx={{ width: "10rem" }}
                        onClick={() => {
                            navigate(-1);
                        }}
                    >
                        Back
                    </Button>
                    <Grid container spacing={2}>
                        {items.value.Items.map((item) => {
                            return (
                                <Grid
                                    xs={12}
                                    md={6}
                                    lg={4}
                                    xl={2}
                                    key={item.Id}
                                >
                                    <ItemContent item={item} />
                                </Grid>
                            );
                        })}
                    </Grid>
                </Stack>
            )}
        </Box>
    );
};

interface ItemContentProps {
    item: Item;
}

const ItemContent: React.FC<ItemContentProps> = ({ item }) => {
    const image = useAsync(async () => {
        return await api.media.getPrimaryImage(item.Id);
    });

    const handleDownload = (itemID: string) => () => {
        api.media.download(itemID);
    };

    return (
        <Paper variant="outlined" sx={{ height: "100%" }}>
            <Container sx={{ height: "100%" }}>
                <Stack sx={{ height: "100%" }} gap={2}>
                    <Link
                        to={item.IsFolder ? `/items/${item.Id}` : ""}
                        style={{ textDecoration: "none" }}
                    >
                        <Box height="8rem">
                            <Typography variant="h6" color="primary">
                                {item.Name}
                            </Typography>
                        </Box>
                        <Box
                            height="400px"
                            width="100%"
                            alignItems="center"
                            justifyContent="center"
                            display="flex"
                        >
                            {!image.loading && (
                                <img
                                    style={{
                                        maxWidth: "100%",
                                        maxHeight: "100%",
                                    }}
                                    src={image.value}
                                />
                            )}
                        </Box>
                    </Link>
                    <Button
                        onClick={handleDownload(item.Id)}
                        variant="outlined"
                    >
                        Download
                    </Button>
                    <SyntaxHighlighter
                        language="json"
                        customStyle={{ flex: 1 }}
                    >
                        {JSON.stringify(item, null, 2)}
                    </SyntaxHighlighter>
                </Stack>
            </Container>
        </Paper>
    );
};

export default ItemsPage;
