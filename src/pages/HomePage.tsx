import React, { useState } from "react";
import { Container, Button, Stack, TextField, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

const HomePage: React.FC = () => {
    const [loginInfo, setLoginInfo] = useState(
        process.env.NODE_ENV === "development"
            ? {
                  url: "http://10.0.0.241:8096",
                  username: "guest",
                  password: "",
              }
            : {
                  url: "",
                  username: "",
                  password: "",
              }
    );

    const navigate = useNavigate();

    const fields = [
        {
            label: "Server URL (eg. http://10.0.0.24:8096)",
            placeholder: "http://10.0.0.24:8096",
            name: "url",
        },
        {
            label: "Username",
            placeholder: "John",
            name: "username",
        },
        {
            label: "Password",
            name: "password",
            type: "password",
        },
    ];

    const handleLogin = async () => {
        console.log(loginInfo);
        const res = await api.auth.login(
            loginInfo.username,
            loginInfo.password,
            loginInfo.url
        );
        console.log(res);
        if (!res.err) {
            navigate("/items");
        }
    };

    return (
        <Container maxWidth="md">
            <Stack gap={3}>
                <Typography variant="h5">Login</Typography>
                {fields.map((field) => (
                    <TextField
                        key={field.name}
                        label={field.label}
                        type={field.type}
                        placeholder={field.placeholder}
                        value={loginInfo[field.name as keyof typeof loginInfo]}
                        onChange={(e) => {
                            setLoginInfo((info) => {
                                const newInfo = { ...info };
                                newInfo[field.name as keyof typeof newInfo] =
                                    e.target.value;
                                return newInfo;
                            });
                        }}
                    />
                ))}
                <Button variant="outlined" onClick={handleLogin}>
                    Login
                </Button>
            </Stack>
        </Container>
    );
};

export default HomePage;
